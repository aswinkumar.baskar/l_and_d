const express=require("express");
const  apiController= require("../controller/user.controller");

const router=express.Router();
//URL for Get All User
router.get("/getuser",apiController.getUser);
module.exports ={router}