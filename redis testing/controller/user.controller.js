const user=require("../model/user.model");
const axios=require("axios");
const redis=require("redis");
const cache=require("../cache/cache");

//controller for get all user
const getUser=async (req,res)=>
{
	

	try
	{	
		var getCache=await cache.client.get("userKey")
    		if(getCache)
    		{
    			console.log("from cache");
    			var z=JSON.parse(getCache);
    			res.status(200).send({"Message":"From Cache","Output":z});	
    		}	
    		else
    		{
    			const getUser=await user.getUser();
    			console.log("from API");
    			var y=JSON.stringify(getUser);
    			await cache.client.set('userKey',y);
    			cache.client.expire('userKey',10);
				res.status(200).json({"Message":"From API","Output":getUser});	
    		}
				
	}
	catch(err)
	{
		console.log(err);
		res.status(400).json({"Exception occured":err,mesage:"from catch"});

	}

}

module.exports={getUser}