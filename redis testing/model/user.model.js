const mongoose=require('mongoose');

const userSchema=mongoose.Schema({
	Name:{type:String,required:true},
	Place:{type:String,required:true},
	Email:{type:String,required:true},
	Age:{type:Number,required:true}
	},
	{timestamps:true}
	);

const model=mongoose.model('USER',userSchema);

const getUser=async ()=>
{
	const user= await model.find();
	return user;
}


module.exports={getUser}
