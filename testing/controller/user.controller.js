const user=require("../model/user.model");
const cache=require("node-cache");
//controller for get all user
const nodecache=new cache({stdTTL:5,checkperiod: 120});
const getUser=async (req,res)=>
{

	try
	{	if(nodecache.has('firstKey'))
		{
			console.log("key found");
			console.log(nodecache.getTtl('firstKey'))
			res.status(200).json({Message:"Success",output:nodecache.get('firstKey')})
		}
		else
		{
		const getUser=await user.getUser();
		nodecache.set('firstKey',getUser,5);
		console.log("key setted")
		console.log(nodecache.getTtl('firstKey'))
		res.status(200).json(getUser)
		}
	}
	catch(err)
	{
		res.status(400).json({"Exception occured":err,mesage:"from catch"});

	}

}

module.exports={getUser}